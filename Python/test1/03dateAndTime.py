def reformatDates(dates):

    """This function recieves a string with "#(th|st|nt) Month YYYY" format, and reformat it to "YYYY-MM-DD" format"""

    months = {"Jan":1, "Feb":2, "Mar":3, "Apr":4, "May":5, "Jun":6, "Jul":7, "Aug":8, "Sep":9, "Oct":10, "Nov":11, "Dec":12}

    DAY   = 0
    MONTH = 1
    YEAR  = 2

    reformated_dates = []
    
    for date in dates:

        date_list = date.split(" ")
        
        day       = (date_list[DAY][:-2]).zfill(2)
        month     = str(months[date_list[MONTH]]).zfill(2)
        year      = date_list[YEAR]

        new_date  = year + '-' + month + '-' + day

        reformated_dates.append(new_date)

    return reformated_dates


if __name__ == '__main__':
    print(reformatDates(["20th Oct 2052",
                         "6th Jun 1933",
                         "26th May 1960",
                         "20th Sep 1958",
                         "16th Mar 2068",
                         "25th May 1912",
                         "16th Dec 2018",
                         "26th Dec 2061",
                         "4th Nov 2030",
                         "28th Jul 1963",]))
        

def longest_chain(w):

    """This function recieves a list of strings and returns an int representing the longest chain"""
    
    words = set() 
    for word in w:
        words.add(word) #Deleting repited words
        
    max_chain = 0
    
    for word in words:
        if len(word) <= max_chain: # skip word if it cannot be greater than max_chain
            continue
        max_candidate = find_longest_chain(word, words, 0, [ 0 ])
        max_chain = max(max_candidate, max_chain) #Compares the maximum of the actual count and the previous
        
    return max_chain

def find_longest_chain(word, words, current_chain, max_chain):

    """This recursive function takes a word and make continuous letters deletions to that word. If the new word is in the words list (words argument) increments current_chain argument by 1. If not, return 0 (base condition). The max_chain argument is used for mantain the current_chain argument value (the array modyfies there value as a pointer in C)."""
    
    if word not in words:
        return 0
    
    current_chain += 1
    max_chain[0] = current_chain #storing the current_chain value with a "pointer"
    
    for i in range(len(word)):
        new_word = word[:i] + word[i+1:] 
        find_longest_chain(new_word, words, current_chain, max_chain)
    return max_chain[0]

if __name__ == "__main__":
    
    w = [ "bdcafg", "bdcaf", "a",  "b",  "ba", "bca", "bda", "bdca" ]
    print(longest_chain(w))

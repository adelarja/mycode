def mergeArray(a, b):

    c = []

    for element1, element2 in zip(a, b):
        c.append(element1)
        c.append(element2)

    return sorted(c)

if __name__ == '__main__':
    a = [1, 2, 3, 4, 5]
    b = [6, 7, 8, 9, 10, 21]
    print(mergeArray(a, b))

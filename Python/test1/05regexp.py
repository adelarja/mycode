import re

grid = "fxie amlo ewbx astu".split()
nrows, ncols = len(grid), len(grid[0])

alphabet = ''.join(set(''.join(grid)))
bogglable = re.compile('[' + alphabet + ']{3,}$', re.I).match

words = set(word.rstrip('\n') for word in open('words') if bogglable(word))
prefixes = set(word[:i] for word in words for i in range(2, len(word)+1))

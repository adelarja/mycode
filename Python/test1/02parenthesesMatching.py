def checkBalances(S):

    """This function recieves an string argument (S) and returns "BALANCED" if the parentheses, brakets and square brakets are opened and closed in correct order or "UNBALANCED" if there are opened and closed in bad order."""

    #The position in open and close lists corresponds with the open and close partners. Example, position zero corresponds with [ in open list and ] in close list. 
    open_list  = ["[", "{", "("] 
    close_list = ["]", "}", ")"]

    stack = []

    for element in S:
        if element in open_list:
            stack.append(element)
        elif element in close_list:
            pos = close_list.index(element)
            #If a close element appears, it has to be the closing element for last element in the stack (LIFO).
            if ((len(stack) > 0) and (open_list[pos] == stack[len(stack)-1])):
                stack.pop()

            else:
                return "UNBALANCED"

    if len(stack) == 0:
        return "BALANCED"

if __name__ == "__main__":
    test = "(hola)[][]{}{}{{{}}}"
    print(checkBalances(test), " (This is a balanced test)")
    test2 = "[[}{}}}"
    print(checkBalances(test2), " (This is an unbalanced test)")

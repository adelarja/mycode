import pdb; pdb.set_trace()

def mergeArrays(a, b):

    """This function recieves two arrays (of the same or different sizes) and merges them."""

    
    #We make 2 for loops to merge arrays of different size. if the arrays were of the same size, we could do for el1, el2 in zip(a, b): c.append(el1) c.append(el2)
    
    c = [element for element in a]
    
    for element in b:
        c.append(element)

    return sorted(c)

if __name__ == '__main__':
    a = [1, 2, 3, 4]
    b = [5, 6, 7, 8, 200, 201, 202]
    print(mergeArrays(a, b))

def getDifferences(differences, arr):

    lastElement = arr[-1]

    for previous in arr[:-1]:
        if lastElement > previous:
            differences.append(lastElement - previous)

def maxDifference(arr):
    """This function takes an integer array as parameter and returns the maximum difference between an element and its lower indexed smaller. If no item has a smaller item with a lower index, then returns -1"""

    differences = []

    i = 2
    while i <= len(arr):
        getDifferences(differences, arr[:i])
        i += 1

    if len(differences) > 0:
        return max(differences)

    else:
        return -1

if __name__ == '__main__':
    print(maxDifference([2, 3, 10, 2, 4, 8, 1]))

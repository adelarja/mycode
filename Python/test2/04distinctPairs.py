def countPairs(arr, k):
    # Write your code here

    pairs = []
    distinct_pairs = []

    for i in range(len(arr)):
        j = i + 1
        while(j < len(arr)):
            if (arr[i] + arr[j]) == k:
                pairs.append(sorted((arr[i], arr[j])))
            j += 1

    for element in pairs:
        if(element not in distinct_pairs):
            distinct_pairs.append(element)

    return len(distinct_pairs)

if __name__ == '__main__':
    print(countPairs([1, 3, 46, 1, 3, 9], 47))

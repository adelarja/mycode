import math

def calculateDenominator(den1, den2):
    """This function is auxiliar to the reducedFunctionSums function. It recieves the two denominators of a fraction sum and returns de denominator of the result"""

    if (den1 % den2) == 0:
        return den1

    elif (den2 % den1 == 0):
        return den2

    else:
        return den1 * den2

def calculateNumAndDens(den1, den2, num1, num2):
    """This function is auxiliar to the reducedFunctionSums function. It recieves the elements of two fractions (denominators and numerators) and returns the denominator of the result and the numerator sum"""

    den = calculateDenominator(den1, den2)
    num1 = (den/den1) * num1
    num2 = (den/den2) * num2

    return int(num1+num2), den

def stringToFraction(fractionString):
    """This functions recieves an string representing a fraction number and returns its numerator and denominator"""

    fractionString = fractionString.split('/')
    num = int(fractionString[0])
    den = int(fractionString[1])

    return num, den

def reduceFraction(num, den):

    gd = math.gcd(num, den)
    while(gd != 1):
        gd = math.gcd(num, den)
        num = int(num/gd)
        den = int(den/gd)

    return num, den


def reducedFractionSums(expressions):

    results = []

    for fract in expressions:

        fractions = fract.split('+')

        fraction1 = fractions[0]
        fraction2 = fractions[1]

        num1, den1 = stringToFraction(fraction1)
        num2, den2 = stringToFraction(fraction2)

        resultNum, resultDen = calculateNumAndDens(den1, den2, num1, num2)
        resultNum, resultDen = reduceFraction(resultNum, resultDen)

        result = str(resultNum) + '/' + str(resultDen)
        results.append(result)

    return results

if __name__ == '__main__':
    a =                  ["722/148+360/176",
                         "978/1212+183/183",
                          "358/472+301/417",
                          "780/309+684/988",
                         "258/840+854/686"]
    for i in a:
        print(reducedFractionSums(a))

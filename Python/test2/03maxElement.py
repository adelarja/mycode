def operateInRange(index1, index2, array, value):

    index1 -= 1
    index2 -= 1

    while(index1 <= index2):
        array[index1] += value
        index1 += 1


def listMax(n, operations):
    # Write your code here

    elements = []

    for i in range(n):
        elements.append(0)

    for operation in operations:
        index1 = operation[0]
        index2 = operation[1]
        sumValue = operation[2]

        operateInRange(index1, index2, elements, sumValue)

        

    return max(elements)

if __name__ == '__main__':
    a = [[1, 2, 10], [2, 4, 5], [3, 5, 12]]
    print(listMax(5, a))

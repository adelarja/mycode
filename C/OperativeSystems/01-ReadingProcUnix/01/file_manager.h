#ifndef FILE_MANAGER_H_
#define FILE_MANAGER_H_

char* read_file        (char*);
int   count_lines      (char*);
void  print_lines      (char*);
char* find_string_line (char*, char*);
char* replace_char     (char*, char, char);
char* get_fdescriptor  (char*);
void remove_substring(char*, const char*);


#endif

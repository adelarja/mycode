#ifndef DEFINES_H_
#define DEFINES_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MAXSIZE       10000000
#define MAXLINESIZE     100000 

//PATHS
#define CPUINFOPATH     "/proc/cpuinfo"
#define KERNELINFOPATH  "/proc/version"
#define UPTIMEPATH      "/proc/uptime"
#define FILESYSTEMPATH  "/proc/filesystems"
#define PROCESSINFOPATH "/proc/self/status"
#define STATPATH        "/proc/stat"
#define HOSTNAMEPATH    "/proc/sys/kernel/hostname"
#define USAGEINFOPATH   "/proc/stat"
#define MEMINFOPATH     "/proc/meminfo"
#define DISKINFOPATH    "/sys/block/sda/stat"
#define LOADAVPATH      "/proc/loadavg"
#define KERNELINFO      "/proc/self/stack"
#define PROC            "/proc"

//KEY WORDS
#define CPUINFO        "model name"
#define VOLCTX         "voluntary_ctxt_switches"
#define INVOLCTX       "nonvoluntary_ctxt_switches"
#define PROCCESS       "processes"
#define CTXTSWTCH      "ctxt"
#define TOTALMEM       "MemTotal"
#define FREEMEM        "MemFree"
#define AVAILABLEMEM   "MemAvailable"
#define STRINGERROR    "-99999"
#define LIMITS         "limits"
#define MAXOPENEDFILES "Max open files"

#endif

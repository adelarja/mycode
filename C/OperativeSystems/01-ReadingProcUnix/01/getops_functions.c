#include "getops_functions.h"
#include "main_program_functions.h"
#include "defines.h"

#define SEPSIZE 100

void manage_option(int argc, char**argv, int option){

  switch(option){

      case 's':
	print_usage_info();
	break;

      case 'l':{
	int ticks;
	int strings;
	sscanf(optarg, "%d", &ticks);
	sscanf(argv[optind], "%d", &strings);
	delay_print(ticks, strings, print_disk_info);
	break;
      }
	
      case 'p':
	print_fdinfo(optarg);
	break;

      case 'f':{
	int id;
	sscanf(optarg, "%d", &id);
	print_proc_limits(id);
	break;
      }

      case 't':
	print_kernel_stack();
	break;


      case '?':
	printf("Bad Argument!!.\n");
	exit(1);

      default:
	printf("ERROR!\n");
	break;
      
  }
}


void print_init_info(){

  print_separator();

  char buffer[MAXLINESIZE];

  get_date(buffer);
  printf("%s\n", buffer);

  get_host_name(buffer);
  printf("%s\n", buffer);
  
  get_cpu_info(buffer);
  printf("%s\n", buffer);
  
  get_kernel_info(buffer);
  printf("%s\n", buffer);

  get_OS_start_time(buffer);
  printf("%s\n", buffer);

  get_fsystem_info(buffer);
  printf("%s\n", buffer);

  print_separator();

}

void print_usage_info(){

  char buffer[MAXLINESIZE];

  print_separator();
  
  get_usage_info(buffer);
  printf("%s\n", buffer);

  get_process_info(buffer);
  printf("%s\n", buffer);

  print_separator();
  
}

void print_disk_info(){
  char buffer[MAXLINESIZE];

  print_separator();
  
  get_disk_info(buffer);
  printf("%s\n", buffer);

  print_separator();
}

void print_fdinfo(char *pid){
  
  static char path[] = "/proc/";
  strcat(path, pid);
  strcat(path, "/fd");
  
  print_separator();
  
  char buffer[MAXLINESIZE];

  get_fdinfo(path, buffer);
  printf("%s\n", buffer);

  print_separator();
  
}

void print_proc_limits(int id){

  char buffer[MAXLINESIZE];
  
  print_separator();

  get_proc_limits(id, buffer);
  printf("%s\n", buffer);

  print_separator();
}

void print_kernel_stack(){

  char buffer[MAXLINESIZE];

  print_separator();
  
  get_kernel_stack(buffer);
  printf("%s\n", buffer);

  print_separator();
  
}

void delay_print(int time, int prints, void (*func_ptr)(void)){

  int counter = 0;

  while((counter += time) < prints){

    (*func_ptr)();
    sleep(time);
    
  }
  
}

void print_separator(){

  printf("\n");
  
  int i = 0;
  
  while(i++ < SEPSIZE)
    printf("%s", "#");

  printf("\n\n\n");
}





#ifndef GETOPFUNC_H
#define GETOPFUNC_H

void manage_option(int, char**, int);
void print_init_info(void);
void print_usage_info(void);
void print_disk_info(void);
void print_fdinfo(char*);
void print_proc_limits(int);
void print_kernel_stack(void);
void print_separator(void);
void delay_print(int, int, void(*)(void));



#endif

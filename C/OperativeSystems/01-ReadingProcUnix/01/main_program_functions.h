#ifndef MAIN_PROG_H_
#define MAIN_PROG_H_

void get_date            (char*);
void get_host_name       (char*);
void get_cpu_info        (char*);
void get_kernel_info     (char*);
void get_OS_start_time   (char*);
void get_usage_info      (char*);
void get_fsystem_info    (char*);
void get_process_info    (char*);
void get_disk_info       (char*);
void get_fdinfo          (char*, char*);
void get_permissions     (char*, char*, char*);
void get_kernel_stack    (char*);
void get_proc_limits     (int, char*);

#endif

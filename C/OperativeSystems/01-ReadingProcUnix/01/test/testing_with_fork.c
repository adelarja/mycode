#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char**argv){

  pid_t child_pid;

  

  child_pid = fork();

  if(child_pid != 0){
    printf("The main program process ID id: %d\n", (int) getpid());
    printf("The child process ID is %d\n", (int) child_pid);
  }

  else
    for(;;)
      ;
  
  
}

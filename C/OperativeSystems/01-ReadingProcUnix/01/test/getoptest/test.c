#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

const char* short_options = "sl:p:f:t";

const struct option long_options[] = {
  {"status", 0, NULL, 's'},
  {"loadav", 2, NULL, 'l'},
  {"pinfo",  1, NULL, 'p'},
  {"fpermv", 1, NULL, 'f'},
  {NULL, 0, NULL, 0}
};

void print_init_info();
void print_usage_info();
void print_disk_info();
void print_fdinfo();
void print_proc_limits(int);
void print_kernel_stack();
void delay_print(int, int, void (*)(void))

int next_option;

int main(int argc, char* argv[]){

  char *prueba;

  print_init_info();

  if(argc > 1){

    while((next_option = getopt_long(argc, argv, short_options, long_options, NULL)) != -1){

      switch(next_option){

      case 's':
	print_usage_info();
	break;

      case 'l':
	delay_print(optarg, argv[optind], print_usage_info);
	break;
	
      case 'p':
	print_fdinfo();
	break;

      case 'f':
	int id;
	sscanf(optarg, "%d", &id);
	print_proc_limits(id);
	break;

      case 't':
	print_kernel_stack();
	break;


      case '?':
	printf("Bad Argument %c.\n", next_option);
	exit(1);

      default:
	printf("ERROR!\n");
	break;
      }
    
    }

  }

  else
    printf("GOOD\n");

  return 0;
  
}

void print_init_info(){

  char buffer[MAXLINESIZE];

  get_date(buffer);
  printf("%s\n", buffer);

  get_host_name(buffer);
  printf("%s\n", buffer);
  
  get_cpu_info(buffer);
  printf("%s\n", buffer);
  
  get_kernel_info(buffer);
  printf("%s\n", buffer);

  get_OS_start_time(buffer);
  printf("%s\n", buffer);

  get_fsystem_info(buffer);
  printf("%s\n", buffer);

}

void print_usage_info(){

  char buffer[MAXLINESIZE];
  
  get_usage_info(buffer);
  printf("%s\n", buffer);

  get_process_info(buffer);
  printf("%s\n", buffer);
  
}

void print_disk_info(){
  char buffer[MAXLINESIZE];

  get_disk_info(buffer);
  printf("%s\n", buffer);
}

void print_fdinfo(){

  char buffer[MAXLINESIZE];

  get_fdinfo("/proc/self/sd", buffer);
  printf("%s\n", buffer);
  
}

void print_proc_limits(int id){

  get_proc_limits(id);
  
}

void print_kernel_stack(){

  char buffer[MAXLINESIZE];

  get_kernel_stack(buffer);
  printf("%s\n", buffer);
  
}

void delay_print(int time, int prints, void (*func_ptr)(void)){

  int counter = 0;

  while((counter += time) < prints){

    print_disk_info();
    sleep(time);
    
  }
  
}





#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>

void strmode(mode_t, char*);

int main(){
  struct stat perm;
  char prueba[200];
  stat("/proc", &perm);
  
  strmode(perm.st_mode, prueba);
  //getmode(prueba, perm->st_mode);

  printf("%s", prueba);

  return 0;
}

void strmode(mode_t mode, char * buf) {
  const char chars[] = "rwxrwxrwx";
  for (size_t i = 0; i < 9; i++) {
    buf[i] = (mode & (1 << (8-i))) ? chars[i] : '-';
  }
  buf[9] = '\0';
}


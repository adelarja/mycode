#include "../defines.h"
#include "../main_program_functions.h"
#include "../file_manager.h"
#include "../getops_functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>


 /*
  //BUSCAR: /proc/[pid]/fdinfo -> file descriptors /proc/[pid]/io /proc/[pid]/limits /proc/[pid]/stack /proc/[pid]/stat->utime, stime, cutime, cstime, starttime  /proc/[pid]/status /proc/stat -> user, nice, idle, system. /proc/loadavg -> promedio de cargas ls -l /proc/self/fd -> para ver file descriptors /sys/block/sda/stat -> para ver peticiones a disco
*/
 
const char* short_options = "sl:p:f:t";

const struct option long_options[] = {
  {"status", 0, NULL, 's'},
  {"loadav", 2, NULL, 'l'},
  {"pinfo",  1, NULL, 'p'},
  {"fpermv", 1, NULL, 'f'},
  {NULL, 0, NULL, 0}
};

int next_option;

int main(int argc, char* argv[]){

  print_init_info();
  
  if(argc > 1)
    while((next_option = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
      manage_option(argc, argv, next_option);
  
  return 0;
  
}

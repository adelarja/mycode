# TODO

## Step: A rdproc, versión inicial
- [x] Tipo y modelo de CPU. **(get_cpu_info)**
- [x] Versión de Kernel.**(get_kernel_info)**
- [x] Cantidad de tiempo transcurrido desde que se inició el sistema operativo, con el formato ddD hh:mm:ss. **(get_OS_start_time)**
- [x] Cantidad de sistemas de archivo soportados por el kernel **(get_fsystem_info)**
- [x] cabecera: Nombre de maquina y fecha + hora actual. **(get_host_name + get_time)**
- [ ] imprimir lo anterior

## Step B: Command line arguments
- [ ] opcion **-s** sin arg (con la siguiente info)
- [x] Cantidad de tiempo de CPU utilizado para usuarios, sistema y proceso idle. **(get_usage_info)**
- [x] Cantidad de cambios de contexto. **(get_usage_info)**
- [x] Número de procesos creados desde el inicio del sistema. **(get_usage_info)**

## Step C:
- [ ] opcion **-l \<a> \<b>**
- [x] Número de peticiones a disco realizadas. **(get_disk_info)**
- [x] Cantidad de memoria configurada en el hardware **(get_disk_info)**
- [x] Cantidad de memoria disponible. **(get_disk_info)**
- [ ] Lista de los promedios de carga de 1 minuto. **(get_disk_info)**

## Step D: Process
- [ ] opcion **-p \<pid>** (con lo siguiente)
- [ ] información sobre los file descriptors a los que tiene acceso un proceso en particular. **(get_fdinfo)**
- [ ] opcion **-f \<pid>** (con lo siguiente)
- [ ] imprimir los límites de archivos abiertos para un cierto proceso
- [ ] opcion **-t** (con lo siguiente:)
- [ ] imprimir el kernel stack trace actual de un cierto proceso. Imprimir solamente el nombre del símbolo
  

## Testing
1. Testear con un programa que abra un archivo y se quede en un loop infinito
2. Utilizando shell:
   Primero, se puede conseguir el process ID usando el comando ps:
   % ps aux | grep processName
   Opcionalmente, se puede instalar y utilizar el command pidof:
   % pidof processName
   Luego, podemos acceder a /proc/\<id>/fd
   Finalmente, un comando que resume esta consigna:
   3% lsof -a -p \<pid>
3. Testear la opción -f con el comando ‘ulimit’.

## Criterios 
- compilar con -Wall -Pedantic
- organizar codigo en modulos razonables
- code style
- sin error y warnings
- sin error de cppcheck

## Breve Explicación de la Arquitectura

Para que el proyecto resulte mantenible, se dividió en los siguientes módulos:

 -> main_program_functions (.c y .h)
 -> file_manager (.c y .h)
 -> getops_functions (.c y .h)
 -> rdproc.c (main)

Las librerías de C comunes a los módulos se incluyen en un módulo llamado defines.h. Para no utilizar "números mágicos" se definieron con directivas      pre-procesador
constantes utilizadas en los módulos. Se definen, entre otras cosas, los paths de los archivos a leer, tamaños máximos de los strings utilizados,    constantes que indi
-can de una manera amigable qué finalidad cumple una determinada función.

### main_program_functions ###
Este módulo contiene las funciones que generan un string con la información requerida para el proyecto rdproc. Las funciones siempre reciben un char* (string) que funci
-ona de buffer. De esa manera, se asegura la utilización de un solo buffer de lectura en el programa principal que recibe la información necesaria para ser formateada e
impresa en pantalla.

### file_manager ###
Este módulo se encarga de la lectura de los archivos y procesamiento de strings. Tiene funciones de lectura de archivos, búsqueda de líneas, reemplazo de caracteres, etc.
El módulo main_program_functions hace uso de las funciones de este módulo para leer los archivos, procesar la información contenida por los archivos y ponerlas en el   bu
-ffer representante de la información solicitada.

### getops_functions ###
Este módulo se encarga del manejo de los argumentos que se entregan al programa principal. Tiene funciones para realizar las repetidas impresiones de pantalla del argumento
-l, y todas las demás funcionalides que tiene el proyecto rdproc. Es el encargado de la impresión en pantalla del buffer de consola. 

### rdproc ###
Se pretende que el main este limpio y fácil de entender. Es por ello que se modularizó el proyecto. El main llama a la función manage_options() de getops_functions y esta
se encarga del resto.


### Módulo de Test forktest ###
Para generar un proceso y conocer su id se utilizó un programa que hace un fork, crea un proceso hijo y nos imprime los id en pantalla para comparar y corroborar la información
que brinda rdproc para las funciones -p y -f.

### TEST RESULTS ###

1) Loop eterno:

Utilizamos forktest. Invocando forktest obtenemos la siguiente impresion de pantalla:

The main program process ID id: 6327

return


The child process ID is 6328


return

Haciendo un ./rdproc -p 6328 obtenemos el siguiente resultado:

dr-x------ .
dr-xr-xr-x ..
lrwx------ 0
lrwx------ 1
lrwx------ 2
l-wx------ 8

Eso nos indica que tenemos archivos de tipo Directory (d) y archivos de tipo link (l). Los archivos son 0, 1, 2 y 8 (. y .. indican el directorio en si mismo y .. el directorio dentro del cual se encuentra).

2) Si ahora hacemos un ls -la /proc/6328/fd obtenemos el siguiente resultado:

dr-x------ 2 adelarja adelarja  0 Aug 31 23:12 .
dr-xr-xr-x 9 adelarja adelarja  0 Aug 31 23:12 ..
lrwx------ 1 adelarja adelarja 64 Aug 31 23:12 0 -> /dev/pts/1
lrwx------ 1 adelarja adelarja 64 Aug 31 23:12 1 -> /dev/pts/1
lrwx------ 1 adelarja adelarja 64 Aug 31 23:12 2 -> /dev/pts/1
l-wx------ 1 adelarja adelarja 64 Aug 31 23:12 8 -> 'pipe:[30957]'

Observamos que los permisos y los nombres estan correctos, por lo que rdproc nos brinda informacion adecuada.

3) Testeo de -f

Haciendo un ./rdproc -f 6328 obtenemos el siguiente resultado:

Max open files            1024                 1048576              files

El numero 1024 corresponde al soft limit y el 1048576 al hard limit (mas adelante se explica que es cada uno).

Si hacemos ulimit -S -n obtenemos el limite soft y -H -n el limite hard. En el primer caso 1024 y en el segundo
1048576.

Si cambiamos el limite -S -n a 1000 y el limite -H -n a 1000000 y luego creamos un nuevo proceso (con forktest)
obtenemos los siguientes resultados.

Invocando forktest:
The main program process ID id: 6580
The child process ID is 6581

Haciendo ahora ./rdproc -f 6581

Max open files            1000                 1000000              files

Esto nos da la pauta de que el programa funciona de manera adecuada con el argumento -f.

Preguntas teóricas:

1) Cuáles son los tipos de type descriptors que podemos encontrar en /proc/<pid>/fd:

Encontrador los tipos d (directorio) y l (link file). Un link file es un archivo que contiene referencia a otro archivo o directorio en la forma de un path absoluto o relativo.
El otro archivo o directorio es el target. Si creamos un link, al modificar el link se modifica el archivo al que apunta el link. Si una vez hecho esto eliminamos el link, el
archivo original permanece. Si no eliminamos el link y eliminamos el archivos al que apunta, el link permanece, pero esta corrupto.

Un hard link puede linkear solamente un archivo, no un directorio. No puede referenciar a un archivo fuera de la unidad de disk o volumen. Los links van a seguir referenciando
al archivo aunque sea movido. Los link simbolicos pueden linkear directorios. Pueden tambien apuntar a un archivo/directorio fuera del volumen. Si el archivo se elimina, el link
sigue existiendo. Si se mueve, el archivo no sigue siendo referenciado por el link. Los links apuntan a nombres de archivo/directorios abstractos (no localizaciones fisicas como el hard).

2) Suponiendo que un usuario está ejecutando el proceso pid 1212, es válida la ejecución del siguiente comando desde una terminal nueva? % echo “Hello, world.” >> /proc/1212/fd/1.

No es valido porque el unico que puede acceder a modificar su fd es el mismo proceso.

3) ### Hard Limit ###

Con ulimit podemos obtener informacion y controlar los recursos del sistema que le brindamos a los procesos. Al hacer ulimit -a obtenemos lo siguiente:

core file size          (blocks, -c) 0
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 62724
max locked memory       (kbytes, -l) 16384
max memory size         (kbytes, -m) unlimited
open files                      (-n) 1024
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 62724
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited

El hard limit es el maximo que se le permite a un usuario, y se setea como superusuario. El valor se setea en /etc/security/limits.conf. El soft limit es el valor efectivo que tiene
en ese momento un usuario. El usuario puede incrementar el soft limit a medida que lo necesita, pero nunca puede superar el hard limit. 
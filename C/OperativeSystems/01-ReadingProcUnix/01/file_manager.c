#include "defines.h"
#include "file_manager.h"

char* read_file(char* name){

  static char buffer[MAXSIZE];
  FILE *fp = fopen(name, "r");
  char a;
  int i = 0;

  while( (( a = getc(fp)) != EOF ) && (i < MAXSIZE) )
    buffer[i++] = a;

  buffer[i] = '\0';

  fclose(fp);

  return buffer;  
}

int count_lines (char* string){

  int i = -1;
  int count = 0;

  while( (i++ < MAXSIZE) && (*string != '\0') )
    if (*string++ == '\n')
      count++;

  return count;
  
}

void print_lines(char* string){

  printf("%s", string);
  
}

char* find_string_line(char* container, char* searched_string){

  static char value[MAXSIZE];
  char* found_string = strstr(container, searched_string);

  int i = 0;

  while( (*found_string != '\n') && (i < MAXSIZE) )
    value[i++] = *found_string++;

  value[i] = '\0';

  return value;
    
}

char* replace_char(char *string, char to_replace, char replace_char){

  char *replaced = string;

  while(*replaced != '\0'){

    if (*replaced == to_replace)
      *replaced = replace_char;

    replaced++;    
  }

  return string;
  
}

char* get_fdescriptor(char *path){

  DIR *dr = opendir(path);

  if (dr == NULL){
    printf("Could not open directory!\n");
    return STRINGERROR;
  }
  
  struct dirent *de;
  static char string[MAXLINESIZE];
  string[0] = ' ';

  while((de = readdir(dr)) != NULL){
    strcat(string, de->d_name);
    strcat(string, " ");
  }

  closedir(dr);

  return string;
  
}

void remove_substring(char *s, const char *toremove){
  while(s = strstr(s, toremove))
    memmove(s, s+strlen(toremove), 1 + strlen(s+strlen(toremove)));
}

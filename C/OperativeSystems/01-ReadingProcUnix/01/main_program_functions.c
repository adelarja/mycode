#include "main_program_functions.h"
#include "file_manager.h"
#include "defines.h"

void get_time(char*, long, char*);
void strmode (mode_t, char*);

void get_date(char *date){

  struct timeval tv;
  struct tm* ptm;
  char time_string[MAXLINESIZE];
  long milliseconds;

  gettimeofday(&tv, NULL);
  ptm = localtime(&tv.tv_sec);
  strftime(time_string, sizeof (time_string), "%d-%m-%Y %H:%M:%S", ptm);
  milliseconds = tv.tv_usec / 1000;
  sprintf(date, "%s.%03ld", time_string, milliseconds);
}

void get_host_name(char *name){

  char *string = read_file(HOSTNAMEPATH);
  sprintf(name, "%s", string);
  
}

void get_cpu_info(char *cpu_info){

  char *string = read_file(CPUINFOPATH);
  sprintf(cpu_info, "%s", find_string_line(string, CPUINFO));
  
}

void get_kernel_info(char *kernel_info){

  char *string = read_file(KERNELINFOPATH);
  sprintf(kernel_info, "%s", string);
  
}

void get_OS_start_time(char *start_time){

  double uptime, idle_time;
  
  char *string = read_file(UPTIMEPATH);
  *start_time = '\0';
  
  sscanf(string, "%lf %lf\n", &uptime, &idle_time);
  get_time("Uptime", (long) uptime, start_time);
  get_time("Idle Time", (long) idle_time, start_time);
  
}

void get_time (char* label, long time, char *start_time)
{
	/* Conversion constants. */
	const long minute = 60;
	const long hour = minute * 60;
	const long day = hour * 24;
	/* Produce output. */
	char auxiliar_string[MAXLINESIZE];
	
	sprintf (auxiliar_string, "%s: %ld days, %ld:%02ld:%02ld ", label, time / day,
	(time % day) / hour, (time % hour) / minute, time % minute);
	strcat(start_time, auxiliar_string);
}

void get_usage_info(char *usage_info){

  char *string = read_file(USAGEINFOPATH);
  char name[MAXLINESIZE];
  long int user;
  long int nice;
  long int system;
  long int idle;

  sscanf(string, "%s %ld %ld %ld %ld", name, &user, &nice, &system, &idle);

  sprintf(usage_info, "%s USER:%ld NICE:%ld SYSTEM:%ld IDLE:%ld \n", name, user, nice, system, idle);

  strcat(usage_info, "Context Switchs: ");
  strcat(usage_info, find_string_line(string, CTXTSWTCH));
  strcat(usage_info,"\nProcesses Created: ");
  strcat(usage_info, find_string_line(string, PROCCESS));
  
}

void get_fsystem_info(char *fsystem_info){

  int filesystems_number;

  char *string = read_file(FILESYSTEMPATH);

  filesystems_number = count_lines(string);

  sprintf(fsystem_info, "File System Cuantity: %d\n", filesystems_number);

  strcat(fsystem_info,"The file system types are: \n");
  strcat(fsystem_info, string);
  remove_substring(fsystem_info, "nodev");
  
}

void get_process_info(char *process_info){
  
  struct rusage usage;
  getrusage (RUSAGE_SELF, &usage);
  sprintf (process_info, "CPU time: %ld.%06ld sec user, %ld.%06ld sec system ",
	  usage.ru_utime.tv_sec, usage.ru_utime.tv_usec,
	  usage.ru_stime.tv_sec, usage.ru_stime.tv_usec);

  char *ctx_switch = read_file(PROCESSINFOPATH);
  
  strcat(process_info, find_string_line(ctx_switch, VOLCTX));
  strcat(process_info, " ");
  strcat(process_info, find_string_line(ctx_switch, INVOLCTX));
  
  char *process_number = read_file(STATPATH);
  strcat(process_info, " The number of processes created is: ");
  strcat(process_info, find_string_line(process_number, PROCCESS));
  
}

void get_disk_info(char* info){

  long int read_info;

  char *string = read_file(DISKINFOPATH);

  sscanf(string, "%ld", &read_info);

  char *disk_info = read_file(MEMINFOPATH);
  
  char total_memory[MAXLINESIZE];
  char free_memory[MAXLINESIZE];
  char available_memory[MAXLINESIZE];

  strcpy(total_memory, find_string_line(disk_info, TOTALMEM));
  strcpy(free_memory, find_string_line(disk_info, FREEMEM));
  strcpy(available_memory, find_string_line(disk_info, AVAILABLEMEM));

  sprintf(info, "Peticiones a Disco: %ld\n%s\n%s\n%s\n", read_info, total_memory, free_memory, available_memory);

  char *load_average = read_file(LOADAVPATH);

  float last_time_avg;
  
  sscanf(load_average, "%f", &last_time_avg);

  *load_average = 0;
  sprintf(load_average, "Promedio ultimo minuto: %f \n", last_time_avg);
  strcat(info, load_average);
  
}

void get_fdinfo(char *path, char *fdinfo){
  
  char fdlist[MAXLINESIZE];
  strcpy(fdlist, get_fdescriptor(path));
  
  get_permissions(fdlist, fdinfo, path);  
}

void get_permissions(char *elements, char *buffer, char *path){
  
    struct stat perm;
    char dir[MAXLINESIZE];
    char new_path[MAXLINESIZE];
    char *iterator = dir;

    buffer[0] = '\0';
    
    strcpy(new_path, path);
    
    while(*elements != '\0'){
      
      while(*elements != ' ')
	*iterator++ = *elements++;

      *iterator = '\0';
      
      if(dir[0] != '\0'){
	strcat(new_path, "/");
	strcat(new_path, dir);
	lstat(new_path, &perm);
	strcpy(new_path, "");
	strcpy(new_path, path);
        strmode(perm.st_mode, buffer);
	strcat(buffer, " ");
	strcat(buffer, dir);
	strcat(buffer, "\n");
      }

      iterator = dir;
      elements++;
    }
  
}

void strmode(mode_t mode, char * buf) {
  //strcat(buf, (S_ISDIR(mode)) ? "d" : "-");
  strcat(buf, (S_ISDIR(mode)) ? "d" : 
	 //(S_ISLNK(mode)) ? "l" :
	 ((mode & S_IFMT) == S_IFLNK) ? "l" :
	      (S_ISCHR(mode)) ? "c" :
	      (S_ISBLK(mode)) ? "b" :
	      (S_ISSOCK(mode))? "s" : "-");
  
    strcat(buf, (mode & S_IRUSR) ? "r" : "-");
    strcat(buf, (mode & S_IWUSR) ? "w" : "-");
    strcat(buf, (mode & S_IXUSR) ? "x" : "-");
    strcat(buf, (mode & S_IRGRP) ? "r" : "-");
    strcat(buf, (mode & S_IWGRP) ? "w" : "-");
    strcat(buf, (mode & S_IXGRP) ? "x" : "-");
    strcat(buf, (mode & S_IROTH) ? "r" : "-");
    strcat(buf, (mode & S_IWOTH) ? "w" : "-");
    strcat(buf, (mode & S_IXOTH) ? "x" : "-");

  /*
  strcat(buf, (mode->d_type == DT_DIR) ? "d" : 
              (mode->d_type == DT_LNK) ? "l" :
              (mode->d_type == DT_CHR) ? "c" :
              (mode->d_type == DT_BLK) ? "b" :
              (mode->d_type == DT_SOCK) ? "s" : "-");
  
    strcat(buf, (mode->st_mode & S_IRUSR) ? "r" : "-");
    strcat(buf, (mode->st_mode & S_IWUSR) ? "w" : "-");
    strcat(buf, (mode->st_mode & S_IXUSR) ? "x" : "-");
    strcat(buf, (mode->st_mode & S_IRGRP) ? "r" : "-");
    strcat(buf, (mode->st_mode & S_IWGRP) ? "w" : "-");
    strcat(buf, (mode->st_mode & S_IXGRP) ? "x" : "-");
    strcat(buf, (mode->st_mode & S_IROTH) ? "r" : "-");
    strcat(buf, (mode->st_mode & S_IWOTH) ? "w" : "-");
    strcat(buf, (mode->st_mode & S_IXOTH) ? "x" : "-");*/
}

void get_kernel_stack(char*kstack){
  char *string = read_file(KERNELINFO);
  strcpy(kstack, string);
}

void get_proc_limits(int pid, char *buffer){
  char path[MAXLINESIZE];

  sprintf(path, "%s/%d/%s", PROC, pid, LIMITS);

  char *string = read_file(path);

  sprintf(buffer, "%s", find_string_line(string, MAXOPENEDFILES));
}

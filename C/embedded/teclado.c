/*
 * teclado.c
 *
 *  Created on: 12/4/2018
 *      Author: root
 */
#include "teclado.h"
#include "header.h"

/*
 * Función que devuelve el valor "verdadero" (TRUE) si se recibe el password
 * correcto por UART. Se utiliza para activar o desactivar la alarma.
 */

bool_t TECLADO_passwordCorrecto(void){

	uint8_t caracter;
	bool_t presionoEnter = ((caracter = BUFFERCIRCULAR_leerCaracter()) == '\n');
	bool_t esCaracterNulo = (caracter == '\0');

	bool_t claveDetectada = FALSE;


	if(!presionoEnter && !esCaracterNulo){
		clave.contrasena[clave.posicionActual] = caracter;
		clave.posicionActual += 1;
	}

	else if(!esNumero(clave.contrasena)){

		TECLADO_restaurarBufferContrasena();

		return claveDetectada;
	}

	else if(presionoEnter){

		(corroborarClave(clave.contrasena)) ? (claveDetectada = TRUE) : (claveDetectada = FALSE);

		TECLADO_restaurarBufferContrasena();


	}

	return claveDetectada;

}

/*
 * Función privada de "teclado.c". Es utilizada para comparar lo que ingresó
 * por UART con la clave pre-seteada. Si es igual, se devuelve TRUE.
 */

static bool_t corroborarClave(uint8_t *clave){

	bool_t claveCorrecta = FALSE;

	(strcmp(clave, pass) == 0) ? (claveCorrecta = TRUE) : (claveCorrecta = FALSE);
	return claveCorrecta;
}

/*
 * Borra el buffer circular de recepción utilizado para recibir datos por el
 * UART.
 */

void TECLADO_restaurarBufferContrasena(void){

	BUFFERCIRCULAR_restaurarBufferRecepcion();
	clearString(clave.contrasena);
	clave.posicionActual = 0;

}




/*
 * bufferCircular.c
 *
 *  Created on: 10/4/2018
 *      Author: root
 */
#include "header.h"
#include "bufferCircular.h"

/*
 * Función que lee de a un byte de UART y lo carga en un buffer circular de
 * recepción.
 */
void BUFFERCIRCULAR_recibirDato(void){
	uint8_t dato;

	if(uartReadByte(UART_USB, &dato)){
		if(dato != '\0' && dato != '\r' )
			cargarBuffer(&bufferRecepcion, dato);
	}
}

/*
 *Función que toma un byte del buffer circular de recepción.
 */

uint8_t BUFFERCIRCULAR_leerCaracter(void){
	return tomarDeBuffer(&bufferRecepcion);
}

/*
 * Carga un String en el buffer circular de envío.
 */

void BUFFERCIRCULAR_solicitarEnvio(uint8_t *string){

	while(*(string) != '\0')
		cargarBuffer(&bufferEnvio, *(string++));
}

/*
 * Función que va tomando de a un byte del buffer circular de envío y lo envía
 * por protocolo UART.
 */

void BUFFERCIRCULAR_enviarDato(void){

	uint8_t dato;

	bool_t existenDatos = ((dato = tomarDeBuffer(&bufferEnvio)) != 0);

	if(existenDatos)
		uartWriteByte(UART_USB, dato);

}

/*
 * Función que resetea el buffer de recepción. Es útil para borrar el buffer
 * cuando se va a solicitar el ingreso de la clave.
 */

void BUFFERCIRCULAR_restaurarBufferRecepcion(void){
	clearString(bufferRecepcion.buffer);
	bufferRecepcion.indicePop = 0;
	bufferRecepcion.indicePush = 0;
}

/*
 * Función privada de bufferCircular. Se utiliza para cargar los buffers
 * circulares de envío/recepción de a un byte.
 */

static void cargarBuffer(bufferCircular *bufferC, uint8_t dato){

	if(((bufferC->indicePush + 1) % BUFFERSIZE) != bufferC->indicePop){
		bufferC->buffer[bufferC->indicePush] = dato;
		bufferC->indicePush += 1;
		bufferC->indicePush = bufferC->indicePush % BUFFERSIZE;
	}

}

/*
 *Se utiliza para tomar datos de los buffers circulares de envío y recepción
 *de a un byte.
 */

static uint8_t tomarDeBuffer(bufferCircular *bufferC){

	uint8_t datoRetorno = 0;

	if(bufferC->indicePop != bufferC->indicePush){
		datoRetorno = bufferC->buffer[bufferC->indicePop];
		bufferC->indicePop += 1;
		bufferC->indicePop = bufferC->indicePop % BUFFERSIZE;
	}

	return datoRetorno;

}


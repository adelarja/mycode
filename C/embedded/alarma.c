#include "header.h"
#include "alarma.h"

/*
 * Máquina de estado de la alarma con los estados:
 *  -> Desactivada
 *  -> Activando
 *  -> Activada
 *  -> Por sonar
 *  -> Sonando
 */

void ALARMA_actualizarEstado(void){

	switch(estadoActual){

	case DESACTIVADA:
	{
		if(nuevoEstado){

			encenderIndicadorLuminoso();

			BUFFERCIRCULAR_solicitarEnvio("Alarma desactivada.\n");
			nuevoEstado = FALSE;

		}

		if(TECLADO_passwordCorrecto()){

			apagarIndicadorLuminoso();

			siguienteEstado(CLAVEINGRESADA, fsmAlarma, &estadoActual);
			nuevoEstado = TRUE;
		}

		break;
	}

	case ACTIVANDO:
	{
		if(nuevoEstado){

			parpadearIndicadorLuminoso();

			BUFFERCIRCULAR_solicitarEnvio("Activando alarma.\n");
			nuevoEstado = FALSE;

			contadorTicks = 0;
			contadorParpadeo = 0;
		}

		if(++contadorParpadeo == TIEMPOPARPADEO){

			parpadearIndicadorLuminoso();

			contadorParpadeo = 0;
		}

		if(++contadorTicks == TIEMPOESPERA){

			apagarIndicadorLuminoso();

			siguienteEstado(SEACTIVO, fsmAlarma, &estadoActual);
			nuevoEstado = TRUE;
		}

		break;
	}

	case ACTIVADA:
	{
		if(nuevoEstado){

			encenderIndicadorLuminoso();

			BUFFERCIRCULAR_solicitarEnvio("Alarma Activada.\n");
			nuevoEstado = FALSE;
		}

		sensorActivado = SENSORES_monitorearVentanas();
		bool_t ventanaAbierta = (sensorActivado == VENTANA1 || sensorActivado == VENTANA2 || sensorActivado == VENTANA3);

		if(ventanaAbierta){

			apagarIndicadorLuminoso();

			TECLADO_restaurarBufferContrasena();

			siguienteEstado(ABRIOVENTANA, fsmAlarma, &estadoActual);
			nuevoEstado = TRUE;

			break;
		}

		if((sensorActivado = SENSORES_monitorearPuerta()) == PUERTA){

			apagarIndicadorLuminoso();

			TECLADO_restaurarBufferContrasena();

			siguienteEstado(ABRIOPUERTA, fsmAlarma, &estadoActual);
			nuevoEstado = TRUE;
		}

		if(TECLADO_passwordCorrecto()){
			apagarIndicadorLuminoso();

			TECLADO_restaurarBufferContrasena();

			siguienteEstado(DESACTIVOINTERIOR, fsmAlarma, &estadoActual);
			nuevoEstado = TRUE;
		}

		break;
	}

	case PORSONAR:
	{
		if(nuevoEstado){
			BUFFERCIRCULAR_solicitarEnvio("Ingrese la Clave de 4 digitos:\n");
			nuevoEstado = FALSE;

			TECLADO_restaurarBufferContrasena();

			contadorParpadeo = 0;
			contadorTicks = 0;
		}

		if(++contadorParpadeo == TIEMPOPARPADEO){
			parpadearIndicadorLuminoso();

			contadorParpadeo = 0;
		}

		if(++contadorTicks == TIEMPOESPERA){
			apagarIndicadorLuminoso();

			siguienteEstado(TIEMPOEXPIRADO, fsmAlarma, &estadoActual);
			nuevoEstado = TRUE;
			contadorTicks = 0;
			contadorParpadeo = 0;
			break;
		}

		if(TECLADO_passwordCorrecto()){
			apagarIndicadorLuminoso();

			siguienteEstado(PASSWORDINGRESADO, fsmAlarma, &estadoActual);
			nuevoEstado = TRUE;
		}


		break;
	}

	case SONANDO:
	{

		if(nuevoEstado){
			BUFFERCIRCULAR_solicitarEnvio("Se disparo la alarma.\n");
			nuevoEstado = FALSE;

			contadorParpadeo = 0;
		}

		if(++contadorParpadeo == TIEMPOPARPADEO){

			parpadearIndicadorLuminoso();

			gpioMap_t indicadorVentanaActiva = indicadorSensorActivo();

			if(indicadorVentanaActiva)
				parpadearIndicadorLuminosoVentana(indicadorVentanaActiva);

			contadorParpadeo = 0;

		}

		if(TECLADO_passwordCorrecto()){

			apagarIndicadorLuminoso();

			gpioMap_t indicadorVentanaActiva = indicadorSensorActivo();
			if(indicadorVentanaActiva)
				apagarIndicadorLuminosoVentana(indicadorVentanaActiva);

			siguienteEstado(PASSWORDINGRESADO, fsmAlarma, &estadoActual);
			nuevoEstado = TRUE;
			break;
		}
		break;
	}

	}

}

/*
 * Función utilizada para encender el indicador luminoso que corresponde al
 * estado en el cual se encuentre la alarma: Ejemplo, en estado activada,
 * enciende el led rojo.
 */

static void encenderIndicadorLuminoso	(void){

	gpioMap_t led = fsmAlarma[estadoActual].salida;
	gpioWrite(led, ON);
}

/*
 * Análoga a la función anterior pero apaga el indicador luminoso.
 */

static void apagarIndicadorLuminoso		(void){

	gpioMap_t led = fsmAlarma[estadoActual].salida;
	gpioWrite(led, OFF);
}

/*
 * Análoga a la función anterior pero hace toggle del indicador luminoso.
 */
static void parpadearIndicadorLuminoso	(void){

	gpioMap_t led = fsmAlarma[estadoActual].salida;
	gpioToggle(led);

}

/*
 * Similar a las funciones anteriores pero actua sobre los indicadores
 * luminosos de las ventanas.
 */

static void parpadearIndicadorLuminosoVentana	(gpioMap_t ventana){
	gpioToggle(ventana);
}

static void apagarIndicadorLuminosoVentana	(gpioMap_t ventana){
	gpioWrite(ventana, OFF);
}

/*
 * Retorna el sensor del cual provino el disparo.
 */
static gpioMap_t indicadorSensorActivo(){
	switch(sensorActivado){

	case VENTANA1: return LED1; break;
	case VENTANA2: return LED2; break;
	case VENTANA3: return LED3; break;
	default: return 0; break;

	}
}

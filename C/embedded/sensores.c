/*
 * sensores.c
 *
 *  Created on: 12/4/2018
 *      Author: root
 */
#include "sapi.h"
#include "fsm.h"
#include "header.h"
#include "sensores.h"

#define VENTANA1 2
#define VENTANA2 3
#define VENTANA3 4
#define PUERTA	 1

/*
 * Devuelve el sensor de ventana que fue disparado. Sirve para indicar con el
 * indicador luminoso la ventana que fue abierta.
 */

uint8_t SENSORES_monitorearVentanas(void){

	if(monitorearSensor(TEC2, &ventana1, &tec2Presionada))
		return VENTANA1;

	if(monitorearSensor(TEC3, &ventana2, &tec3Presionada))
		return VENTANA2;

	if(monitorearSensor(TEC4, &ventana3, &tec4Presionada))
		return VENTANA3;

	return 0;

}

/*
 * Se utiliza para saber si el sensor disparado fue la puerta. Si se abre la
 * puerta, se da la posibilidad de desactivar la alarma antes de que se
 * dispare.
 */

uint8_t SENSORES_monitorearPuerta(void){

	if(monitorearSensor(TEC1, &puerta, &tec1Presionada))
		return PUERTA;

	return 0;

}

/*
 * Máquina de estado antirrebote para los sensores. Se ingresa cada 50 mS y por
 * ende funciona el antirrebotes.
 */

static bool_t monitorearSensor(gpioMap_t tecla, uint16_t *ventana, bool_t *teclaPresionada){

	bool_t entrada = !gpioRead(tecla);
	siguienteEstado(entrada, fsmAntirrebote, ventana);

	bool_t ventanaAbierta = FALSE;

	switch(*ventana){

		case OUTON:
			if(!(*teclaPresionada)){

				*teclaPresionada = ON;
				ventanaAbierta = TRUE;
			}
			break;

		case OUTOFF:
			ventanaAbierta = FALSE;
			*teclaPresionada = OFF;
			break;

		case UNDEFINED:
			ventanaAbierta = FALSE;
			break;

		}

	return ventanaAbierta;
}

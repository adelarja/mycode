/*==================[inlcusiones]============================================*/
#include "sapi.h"        // <= Biblioteca sAPI
#include <stdio.h>
#include "header.h"
#include "cooperativeOs_isr.h"
#include "cooperativeOs_scheduler.h"

#define ACTUALIZARALARMA	50
#define RECIBIRDATO			10
#define ENVIARDATO			10
//------------------------------------------------------//

//-------------------CONFIGURACIÓN UART----------------//
DEBUG_PRINT_ENABLE
//------------------------------------------------------//

int main( void ){


	boardConfig();

	debugPrintConfigUart( UART_USB, 115200 );
	debugPrintlnString( "UART_USB configurada.\n\r" );

	schedulerInit();

	schedulerAddTask((sAPI_FuncPtr_t) BUFFERCIRCULAR_recibirDato,		0,			  	 		RECIBIRDATO);
	schedulerAddTask((sAPI_FuncPtr_t) ALARMA_actualizarEstado,		    1,   			   ACTUALIZARALARMA);
	schedulerAddTask((sAPI_FuncPtr_t) BUFFERCIRCULAR_enviarDato,		2,   			         ENVIARDATO);


	schedulerStart(1);

	while( TRUE )
	{
		schedulerDispatchTasks();
	}
	return 0;
}


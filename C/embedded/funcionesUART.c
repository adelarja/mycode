/*
 * funcionesUART.c
 *
 *  Created on: 8/4/2018
 *      Author: root
 */
#include "header.h"

void recibirStringUART(uint8_t *buffer){

	uint8_t receiveByte;

	while(uartReadByte( UART_USB, &receiveByte ) != FALSE){
		if(receiveByte != '\n' && receiveByte != '\r' )
		*(buffer++) = receiveByte;
	}

	*buffer = '\0';

}

uint8_t detectarComando(uint8_t *rx){

	uint8_t comando[] = "TEC1";

	cargarString(comando, "TEC1");
	if(compararStrings(rx, comando)) return TEC1;

	cargarString(comando, "TEC2");
	if(compararStrings(rx, comando)) return TEC2;

	cargarString(comando, "MENU");
	if(compararStrings(rx, comando)) return 'M';

	if(*rx == 'T')
		return 'T';

	return 'E';
}

bool_t compararStrings(uint8_t *string1, uint8_t *string2){

	while((*string1 == *string2)){

		if(*string1 == '\0'){
			return TRUE;
		}

		string1++;
		string2++;
	}

	return FALSE;

}

void cargarString(uint8_t *comando, uint8_t string[]){

	uint8_t i = 0;

	while(string[i] != '\0'){
		*(comando++) = string[i++];
	}

	*comando = '\0';
}

void clearString(uint8_t *string){
	while(*string != '\0'){
		*(string++) = '\0';
	}
}

uint32_t castStringToInt(uint8_t *rx){
	uint32_t numeroCasteado = 0;

	while(*rx != '\0'){
		numeroCasteado *= 10;
		numeroCasteado += *rx - 0x30;
		rx++;
	}

	return numeroCasteado;
}

bool_t esNumero(uint8_t *rx){

	while(*rx != '\0'){
		switch(*(rx++)){

		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			break;

		default:
			return FALSE;
			break;

		}
	}

	return TRUE;

}

void makeMenu(void){

	uint8_t menu[] =
			"\n\r"
			"===================================MENU===================================\n\r"
			"\n\r"
			"Tipee TEC1 para cambiar el periodo de parpadeo del led.\n\r"
			"Tipee TEC2 para cambiar el led activo.\n\r"
			"Tipee T seguido del periodo de parpadeo deseado para establecer un periodo "
			"\n\rde parpadeo. Ejemplo: T1000 establece un periodo de 1000 mS\n\r"
			"==========================================================================\n\r"
			;

	uartWriteString(UART_USB, menu);

}



#include "header.h"

//Función que retorna la salida de un estado particular
uint16_t obtenerSalida		(estado state){
	return state.salida;
}

//Función que cambia el estado de la máquina de estado de acuerdo a la entrada.
void siguienteEstado	    (bool_t entrada, estado fsm[], uint16_t *estadoActual){
	*estadoActual = fsm[*estadoActual].proximoEstado[entrada];
}

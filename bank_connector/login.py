from selenium import webdriver
import time
import getpass
driver = webdriver.Firefox()
driver.get("https://www.macro.com.ar/bancainternet/#")

username = input("Ingrese nombre de usuario:\n")
password = getpass.getpass(prompt="Ingrese contraseña:\n")

driver.find_element_by_xpath("//input[@type='password']").send_keys(username)
driver.find_element_by_id("processCustomerLogin").click()

time.sleep(5)

driver.find_element_by_xpath("//input[@type='password']").send_keys(password)
driver.find_element_by_id("processSystem_UserLogin").click()

